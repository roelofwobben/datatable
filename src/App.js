import './App.css';
import { useState, useEffect } from "react";
import DataTable from './components/DataTable'


function App() {

  const [posts, setPosts] = useState([]); 

  useEffect( () => {
    fetchData();
  }, []); 
  
  const fetchData = async () => {
    let res = await fetch('https://jsonplaceholder.typicode.com/posts')
    let json = await res.json();
    setPosts(json);   
  }

  return (
    <div>
      {posts?.length > 0 && <DataTable posts={posts}/>}
    </div>
  )
}



export default App;
