import React, { useState } from 'react';
import Pagination from './Pagination';

function DataTable(props) {

  const [filteredPosts, setFilteredPosts] = useState(props.posts);
  const [sortProp, setSortProp] = useState({dir :"asc", column : ""});  
  const [currentPage, setCurrentPage] = useState(1);
  const [searchTerm, setSearchTerm] = useState("")
  
  let PageSize = 10;
  const firstPageIndex = (currentPage - 1) * PageSize;
  const lastPageIndex = firstPageIndex + PageSize;

  const populateFilteredPosts = (searchTerm, currentPage) =>{
    let filteredPosts = [];
    if(searchTerm){
      filteredPosts =  props.posts.filter(post => post.title.toLowerCase().includes(searchTerm.toLowerCase()) || post.body.includes(searchTerm.toLowerCase()))
    }
    else {
      filteredPosts = props.posts;
    }
    filteredPosts.sort((a, b) => {
        const first = a[sortProp.column];
        const second = b[sortProp.column];
        if (sortProp.dir === "asc") {
            if (first > second) {
                return 1;
            } else if (second > first) return -1;
            return 0;
        } else {
            if (first < second) {
                return 1;
            } else if (second < first) return -1;
            return 0;
        }})
        const PageSize = 10;
        const firstPageIndex = (currentPage - 1) * PageSize;
        const lastPageIndex = firstPageIndex + PageSize;
      
        const paginatedPosts = filteredPosts.slice(firstPageIndex, lastPageIndex);
    setFilteredPosts(paginatedPosts);
  }

  const searchItems2 = (event) =>  {
    setSearchTerm(event.target.value);
    populateFilteredPosts(sortProp, searchTerm, currentPage);  
  }

  const setSort2 = (column) => {
    setSortProp( {
      column:  column, 
      dir : (sortProp.dir === "asc" ? "desc" : "asc") 
    })
    populateFilteredPosts(sortProp, searchTerm, currentPage); 
  }

  const searchItems = (event) => {
    const searchTerm = event.target.value;
    if(searchTerm){
      setFilteredPosts(props.posts.filter(post => post.title.toLowerCase().includes(searchTerm.toLowerCase()) || post.body.includes(searchTerm.toLowerCase())))
    }
    else
    {
      setFilteredPosts(props.posts)
    }
  }

  const setSort = (column) => {
    setSortProp( {
      column:  column, 
      dir : (sortProp.dir === "asc" ? "desc" : "asc") 
    })
    filteredPosts.sort((a, b) => {
      const first = a[sortProp.column];
      const second = b[sortProp.column];
      if (sortProp.dir === "asc") {
          if (first > second) {
              return 1;
          } else if (second > first) return -1;
          return 0;
      } else {
          if (first < second) {
              return 1;
          } else if (second < first) return -1;
          return 0;
      }})
  }

  return (
    <div class="container">
      <div class="actions">
        <div class="filter">
          <label for="standard-select">Show</label>
          <div class="select">
              <select name="show" id="standard-select" class="form-control">
                <option value="10">10</option>
              </select>
            </div>
          <p> posts</p> 
        </div>
        <div className="search">
            <input type="text" class="form-control" placeholder="Search Posts" onChange={(event) => searchItems2(event)}></input>
        </div>
        <div className="data">
          <div className="header">
            <p className="text_big">title <i class="fa-solid fa-filter" onClick={() => setSort2("title")}></i></p>
            <p className="text_big">body <i class="fa-solid fa-filter" onClick={() => setSort2("body")}></i></p>
            <p className="text_small">userid <i class="fa-solid fa-filter" onClick={() => setSort2("userId")}></i></p>
          </div>
          <div className="lines">
            {filteredPosts.slice(firstPageIndex, lastPageIndex).map ( (item) =>
              (<div class="line"> 
               <p className='text_big'>{item.title}</p>         
               <p className="text_big">{item.body}</p>
               <p className="text_small">{item.userId}</p>
              </div> )
              )
             }
             <Pagination
        className="pagination-bar"
        currentPage={currentPage}
        totalCount={filteredPosts.length}
        pageSize={PageSize}
        onPageChange={page => setCurrentPage(page)}
      />
          </div>
        </div>
      </div>
    </div>
  )
}

export default DataTable
